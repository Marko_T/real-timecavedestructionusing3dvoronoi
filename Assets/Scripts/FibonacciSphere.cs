﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

//Generate points on a spheric surface with fibonaccy disribution used to add new points when mining
public class FibonacciSphere {

	public static List<DoubleVector> FibonacciSpherePoints(DoubleVector center, double radius,  int samples)
    {
        double rnd = 1;

        rnd = new System.Random().NextDouble();
        List<DoubleVector> points = new List<DoubleVector>();

        double offset = 2d / samples;
        double increment = Math.PI * (3d - Math.Sqrt(5d));

        for(int i = 0; i < samples; i++)
        {
            double y = ((i * offset) - 1) + (offset / 2d);
            double r = Math.Sqrt(1 - y * y);

            double phi = ((i + rnd) % samples) * increment;

            double x = Math.Cos(phi) * r;
            double z = Math.Sin(phi) * r;

            points.Add(new DoubleVector(x, y, z)*radius + center);
        }
        return points;
    }
}
