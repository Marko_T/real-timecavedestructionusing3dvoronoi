﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class DelaunayControl : MonoBehaviour {

    [SerializeField]
    private int pointCount = 10;

    [SerializeField]
    private GameObject player;

    private BowyerWatson bw;
    // Use this for initialization

    private bool _drawDelaunay = false;
    private bool _drawVoronoi = false;

	void Start () {
        UnityEngine.Random.InitState(11);
        float start = Time.realtimeSinceStartup;
        bw = new BowyerWatson(pointCount);
        bw.MakeDiagram();
        Debug.Log(Time.realtimeSinceStartup - start);
        createCells();

	}
	
	// Update is called once per frame
	void Update () {
        if (_drawDelaunay) bw.DrawTriangulation();
        if (_drawVoronoi) bw.DrawVoronoi();
	}

    public void drawTriangulation()
    {
        _drawDelaunay = !_drawDelaunay;
    }

    public void drawVoronoi()
    {
        _drawVoronoi = !_drawVoronoi;
    }

    //Create the initial cave caviti where the player is placed.
    public void createCells()
    {
        DelaunayVertex v = bw.vertices.OrderBy(x => (x.loc - new DoubleVector(5,5,5)).sqrMagnitude).ToList()[0];
        player.transform.position = v.loc.vector3;
        v.CalculateVoronoiCell();
        v.cell.destroyed = true;

        List<DelaunayVertex> neighbours = new List<DelaunayVertex>();
        foreach(DelaunayVertex n in v.cell.neighbours)
        {
            n.CalculateVoronoiCell();
            n.cell.destroyed = true;
            neighbours.AddRange(n.cell.neighbours);
        }

        neighbours = neighbours.Distinct().ToList();

        neighbours.Remove(v);
        neighbours.RemoveAll(x => v.cell.neighbours.Contains(x));
        foreach(DelaunayVertex n in neighbours)
        {
            n.CalculateVoronoiCell();
            n.CreateCell();
        }
             

    }
}
