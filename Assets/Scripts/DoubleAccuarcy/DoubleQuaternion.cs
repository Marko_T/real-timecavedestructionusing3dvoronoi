﻿using System;


//Lightweight double accuaraccy quaternion
public struct DoubleQuaternion  {


    double w, x, y, z;

    public DoubleQuaternion normalized
    {
        get
        {
            double mag = Math.Sqrt(x * x + y * y + z * z + w * w);
            return new DoubleQuaternion(x/mag, y/mag, z/mag, w/mag);
        }
    }
    public DoubleQuaternion inverse
    {
        get
        {
            return new DoubleQuaternion(-x, -y, -z,w);
        }
    }

    public DoubleQuaternion(double x, double y, double z, double w)
    {
        this.w = w;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public DoubleQuaternion(double angle, DoubleVector axis)
    {
        double cosA = DoubleVector.Dot(axis, new DoubleVector(1, 0, 0)) / axis.magnitude;
        double cosB = DoubleVector.Dot(axis, new DoubleVector(0, 1, 0)) / axis.magnitude;
        double cosC = DoubleVector.Dot(axis, new DoubleVector(0, 0, 1)) / axis.magnitude;

        w = Math.Cos(angle / 2);
        x = Math.Sin(angle / 2) * axis.x;
        y = Math.Sin(angle / 2) * axis.y;
        z = Math.Sin(angle / 2) * axis.z;
    }

    public static DoubleVector operator *(DoubleQuaternion rotation, DoubleVector point)
    {
        double num1 = rotation.x * 2;
        double num2 = rotation.y * 2;
        double num3 = rotation.z * 2;
        double num4 = rotation.x * num1;
        double num5 = rotation.y * num2;
        double num6 = rotation.z * num3;
        double num7 = rotation.x * num2;
        double num8 = rotation.x * num3;
        double num9 = rotation.y * num3;
        double num10 = rotation.w * num1;
        double num11 = rotation.w * num2;
        double num12 = rotation.w * num3;
        DoubleVector vector3 = new DoubleVector();
        vector3.x = ((1.0 - ((double)num5 + (double)num6)) * (double)point.x + ((double)num7 - (double)num12) * (double)point.y + ((double)num8 + (double)num11) * (double)point.z);
        vector3.y = (((double)num7 + (double)num12) * (double)point.x + (1.0 - ((double)num4 + (double)num6)) * (double)point.y + ((double)num9 - (double)num10) * (double)point.z);
        vector3.z = (((double)num8 - (double)num11) * (double)point.x + ((double)num9 + (double)num10) * (double)point.y + (1.0 - ((double)num4 + (double)num5)) * (double)point.z);
        return vector3;
    }

    public static DoubleQuaternion operator *(DoubleQuaternion lhs, DoubleQuaternion rhs)
    {
        return new DoubleQuaternion(
            lhs.w * rhs.x + lhs.x * rhs.w + lhs.y * rhs.z - lhs.z * rhs.y, 
            lhs.w * rhs.y + lhs.y * rhs.w + lhs.z * rhs.x - lhs.x * rhs.z,
            lhs.w * rhs.z + lhs.z * rhs.w + lhs.x * rhs.y - lhs.y * rhs.x,
            lhs.w * rhs.w - lhs.x * rhs.x - lhs.y * rhs.y - lhs.z * rhs.z
        );
    }

    public override string ToString()
    {
        return "(" + w + ", " + x + ", " + y + ", " + z + ")";
    }

}
