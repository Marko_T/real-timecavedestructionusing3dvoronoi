﻿using System;
using System.Runtime.CompilerServices;

//Plane with double accuaraccy
namespace UnityEngine
{
    public struct Planed
    {

        private DoubleVector _Normal;
        private double _Distance;

        public DoubleVector normal
        {
            get
            {
                return this._Normal;
            }set
            {
                this._Normal = value;
            }
        }

        public double distance
        {
            get
            {
                return this._Distance;
            }set
            {
                this._Distance = value;
            }
        }

        public Planed(DoubleVector inNormal, DoubleVector inPoint)
        {
            this._Normal = inNormal.normalized;
            this._Distance = -DoubleVector.Dot(inNormal, inPoint);
        }

        public bool GetSide(DoubleVector inPt)
        {
            return DoubleVector.Dot(this.normal, inPt) + this.distance > 0.0;
        }

        public double GetDistanceToPoint(DoubleVector inPt)
        {
            return DoubleVector.Dot(this.normal, inPt) + this.distance;
        }
    }
}
