﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour {

    public RectTransform crosshair;
    public RectTransform menupanel;
    public RectTransform controls;
    public UIHandler ui;

    private bool controlsOpen = false;

    private void Start()
    {
        menupanel.GetChild(0).GetComponent<Button>().Select();
    }

    public void StartSimulation()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Main");
    }

    public void ExitApplication()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit ();
#endif
    }

    public void showMenu()
    {
        crosshair.gameObject.SetActive(!crosshair.gameObject.active);
        menupanel.gameObject.SetActive(!menupanel.gameObject.active);

        Time.timeScale = 0;
        menupanel.GetChild(0).GetComponent<Button>().Select();
    }

    public void ContinueSimulation()
    {
        if (controlsOpen)
        {
            CloseControls();
        }
        else
        {
            crosshair.gameObject.SetActive(!crosshair.gameObject.active);
            menupanel.gameObject.SetActive(!menupanel.gameObject.active);
            ui.paused = false;
            Time.timeScale = 1;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }

    public void ShowControls()
    {
        controls.gameObject.SetActive(!controls.gameObject.active);
        controls.GetComponentInChildren<Button>().Select();
        controlsOpen = true;
    }

    public void CloseControls()
    {
        controls.gameObject.SetActive(!controls.gameObject.active);
        menupanel.GetChild(2).GetComponent<Button>().Select();
        controlsOpen = false;
    }

}
