﻿using UnityEngine;
using UnityEngine.UI;

public class UIHandler : MonoBehaviour {

    public RectTransform crosshair;
    public RectTransform menupanel;

    public bool paused = false;
	void Update () {
        if (Input.GetButtonDown("Menu"))
        {
            if (!paused)
            {
                paused = true;
                menupanel.GetComponent<MenuController>().showMenu();
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
            else
            {
                paused = false;
                menupanel.GetComponent<MenuController>().ContinueSimulation();
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
        }
	}
}
